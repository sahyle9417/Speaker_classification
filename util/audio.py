import numpy as np
import librosa
from hparams import hparams

def load_wav(path):
    return librosa.core.load(path, sr=hparams.sample_rate)[0]

def extract_feature(wav_file):
    X = load_wav(wav_file)
    #X_stret = librosa.effects.time_stretch(X, 0.5)
    #X_pitch = librosa.effects.pitch_shift(X_stret, sample_rate, n_steps=4)
    #above lines may improve feature_extracting
    stft = np.abs(librosa.stft(X))
    mfccs = np.mean(librosa.feature.mfcc(y=X, sr=hparams.sample_rate, n_mfcc=40).T, axis=0)             # 40
    chroma = np.mean(librosa.feature.chroma_stft(S=stft, sr=hparams.sample_rate).T, axis=0)             # 12
    mel = np.mean(librosa.feature.melspectrogram(X, sr=hparams.sample_rate).T, axis=0)                  # 128
    contrast = np.mean(librosa.feature.spectral_contrast(S=stft, sr=hparams.sample_rate).T, axis=0)     # 7
    tonnetz = np.mean(librosa.feature.tonnetz(y=librosa.effects.harmonic(X), sr=hparams.sample_rate).T, axis=0)      # 6
    features = np.hstack((mfccs, chroma, mel, contrast, tonnetz))                                       # 193
    return features