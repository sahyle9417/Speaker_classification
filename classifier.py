import io
import numpy as np
import tensorflow as tf
from hparams import hparams
from librosa import effects
from models import create_model
from util import audio
from datafeeder import DataFeeder


class Classifier:
  def load(self, checkpoint_path, num_of_class):
    print('Construct model')
    input_features = tf.placeholder(tf.float32, [None, hparams.wav_features], 'input_features')
    with tf.variable_scope('model') as scope:
      self.model = create_model(hparams)
      self.model.initialize(input_features=input_features, num_of_class=num_of_class)
      self.outputs = self.model.outputs
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    self.session = tf.Session(config=config)
    self.session.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    saver.restore(self.session, checkpoint_path)
    print('Checkpoint loaded from: %s' % checkpoint_path)


  def classify(self, input_features):
    feed_dict = {
      self.model.input_features: input_features
    }
    predicted_classes = self.session.run(self.outputs, feed_dict=feed_dict)
    return predicted_classes