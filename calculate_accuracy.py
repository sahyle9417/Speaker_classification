import argparse
import os
import glob
import numpy as np



def calculate_accuracy(args):
  input_file = args.input_file
  output_file = args.output_file
  if output_file is None:
    output_file = os.path.join(os.path.dirname(input_file),'accuracy.csv')
  elif os.path.isdir(os.path.dirname(args.output_file)):
    os.makedirs(os.path.dirname(args.output_file), exist_ok=True)

  contents = []
  with open(input_file, encoding='utf-8') as f:
    for line in f:
      cells = line.strip().split('|')
      cells.append('')
      contents.append(cells)

  for i in range(len(contents)):
    if contents[i][1] in contents[i][0]:
      contents[i][2] = 'TRUE'
    else:
      contents[i][2] = 'FALSE'

  correct = 0
  for i in range(len(contents)):
    if contents[i][2] == 'TRUE':
      correct += 1

  accuracy = correct/len(contents)
  print("total={}".format(len(contents)))
  print("correct={}".format(correct))
  print("accuracy={0:.3f}%".format(accuracy*100))

  first_line = np.array([['total='+str(len(contents)),'correct='+str(correct),'accuracy='+'{0:.3f}%'.format(accuracy*100)]])
  contents = np.asarray(contents)
  contents = np.concatenate((first_line, contents), axis=0)

  csv_writer = open(output_file, "w")
  for i in range(len(contents)):
    csv_writer.write(str.format("%s|%s|%s\n" % (contents[i][0], contents[i][1], contents[i][2])))
  csv_writer.close()
  print('Output csv file is saved to %s' % output_file)


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-in', '--input_file', required=True, default='classification_result.csv')
  parser.add_argument('-out', '--output_file', default=None)
  args = parser.parse_args()
  calculate_accuracy(args)


if __name__ == '__main__':
  main()
