# coding: utf-8

from __future__ import print_function
from __future__ import division

import h5py

import re
from io import BytesIO
from pydub import AudioSegment

import os
import sys
import logging
import tqdm
import argparse

from hparams import hparams  # dependency 제거

logger = logging.getLogger()
logging.basicConfig(stream=sys.stdout, format='%(levelname)s:%(name)s:%(asctime)s:%(message)s', level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('-in','--input_h5file', type=str, required=True,
                    help='h5 file path e.g. /data/yuinna.h5')
parser.add_argument('-out', '--out_dir', type=str, required=True,
                    help='dataset dir e.g. ~/Data/Yuinna')
parser.add_argument('-inft', '--input_filter', type=str, default=None,
                    help='file name filter')

# erase
parser.add_argument('-koronly', '--korean_only', action='store_true',
                    help='If set, choose sentence only with korean character.')


parser.add_argument('-de', '--debug', type=bool, default=True,
                    help="debug print")

args = parser.parse_args()
if args.debug:
    logger.setLevel(logging.CRITICAL)

output_db = h5py.File(args.input_h5file, 'r')

audio_db = output_db.require_group('audio')

audio_list = []
def func_find_dataset(name, obj):
    if isinstance(obj, h5py.Dataset):
        if args.input_filter is None:
            audio_list.append(name)
        else:
            if re.search(args.input_filter, name):
                audio_list.append(name)

audio_db.visititems(func_find_dataset)

total_audio = len(audio_list)
logger.info('---*--- %d audios (%s)', total_audio, audio_list[:10])

os.makedirs(args.out_dir, exist_ok=True)

for idx, datasetname in enumerate(tqdm.tqdm(audio_list)):
    logger.info('   Audio %s (%d/%d)', datasetname, idx, total_audio)

    segment_audio = audio_db.get(datasetname)
    audio_data = segment_audio[0]

    audio_f = BytesIO(audio_data.tobytes())
    audio_f.seek(0)
    audio = AudioSegment.from_file(audio_f)
    audio = audio.set_frame_rate(hparams.sample_rate)
    audio_file = datasetname.replace('/', '-')
    audio_tempfile = os.path.join(args.out_dir,'%s.wav' % (audio_file))
    with open(audio_tempfile, 'w+b') as audio_tf:
        audio.export(out_f=audio_tf, format='wav')

output_db.close()
exit(0)
