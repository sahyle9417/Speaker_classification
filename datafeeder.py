import numpy as np
import os
import glob
import random
import tensorflow as tf
import threading
import time
import traceback
from util.infolog import log
from util import audio


group_label = {0 : ['0', 'male', 'Male', 'man', 'Man'],
               1 : ['1', 'female', 'Female', 'woman', 'Woman'],
               }

# feeder = DataFeeder(coord, feat_dir, class_names, hparams)
class DataFeeder(threading.Thread):
  '''Feeds batches of data into a queue on a background thread.'''

  def __init__(self, coordinator, feat_dir, class_names, hparams):
    super(DataFeeder, self).__init__()
    self._coord = coordinator
    self._hparams = hparams
    self._offset = 0

    # Load metadata:
    self._feat_dir = feat_dir
    self._npz_files = glob.glob(os.path.join(feat_dir, '*.npz'))
    self._feat_length = hparams.wav_features
    self.class_names = class_names
    self.num_of_class = len(class_names)

    # Create placeholders for inputs and targets. Don't specify batch size because we want to
    # be able to feed different sized batches at eval time.
    self._placeholders = [
      tf.placeholder(tf.float32, [None, self._feat_length], 'input_features'),
      tf.placeholder(tf.int32, [None], 'targets')
    ]

    # Create queue for buffering data:
    queue = tf.FIFOQueue(8, [tf.float32, tf.int32], name='input_queue')
    self._enqueue_op = queue.enqueue(self._placeholders)
    self.input_features, self.targets = queue.dequeue()

    self.input_features.set_shape(self._placeholders[0].shape)
    self.targets.set_shape(self._placeholders[1].shape)

  def start_in_session(self, session):
    self._session = session
    self.start()


  def run(self):
    try:
      while not self._coord.should_stop():
        self._enqueue_next_group()
    except Exception as e:
      traceback.print_exc()
      self._coord.request_stop(e)


  def _enqueue_next_group(self):
    start = time.time()

    # Read a group of examples:
    batch_size = self._hparams.batch_size
    batches_per_group = self._hparams.batches_per_group
    examples = [self._get_next_example() for i in range(batch_size * batches_per_group)]

    batches = [examples[i:i+batch_size] for i in range(0, len(examples), batch_size)]
    random.shuffle(batches)

    log('Generated %d batches of size %d in %.03f sec' % (len(batches), batch_size, time.time() - start))
    for batch in batches:
      feed_dict = dict(zip(self._placeholders, _prepare_batch(batch)))
      self._session.run(self._enqueue_op, feed_dict=feed_dict)


  def _get_next_example(self):
    '''Loads a single example from disk'''
    if self._offset >= len(self._npz_files):
      self._offset = 0
      random.shuffle(self._npz_files)
    npz_filepath = self._npz_files[self._offset]
    npz_file = np.load(npz_filepath)
    target = npz_file['label'][0]
    feature = npz_file['feature']
    self._offset += 1
    return (feature, target)


def _prepare_batch(batch):
  random.shuffle(batch)
  input_features = [x[0] for x in batch]
  target = np.asarray([x[1] for x in batch], dtype=np.int32)
  return (input_features, target)