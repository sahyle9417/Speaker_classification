import argparse
import os
import sys
import math
import glob
from tqdm import tqdm
import numpy as np
from hparams import hparams
from util import infolog
from util import audio
from classifier import Classifier

log = infolog.log

def inference(args):
  os.makedirs(args.out_dir, exist_ok=True)

  log('Extracting features from wav..')
  wav_filenames = []
  for wav_filename in glob.glob(os.path.join(args.input_dir, '*.wav')):
      wav_filenames.append(os.path.basename(os.path.splitext(wav_filename)[0]))
  wav_filenames.sort()
  wav_features = []
  for i in tqdm(range(len(wav_filenames))):
    wav_features.append(audio.extract_feature(os.path.join(args.input_dir, wav_filenames[i]+'.wav')))
  log('Features extracted')

  log('Loading class information from %s' % (os.path.join(args.checkpoint_dir, 'class_info.csv')))
  class_info = []
  with open(os.path.join(args.checkpoint_dir, 'class_info.csv'), "r") as f:
    for line in f:
      single_line = line.strip().split('|')
      class_info.append(single_line)
  num_of_class = len(class_info)

  # choose last checkpoint
  if args.inference_step is None or args.inference_step == -1:
    with open(os.path.join(args.checkpoint_dir, "checkpoint"), "r") as f:
      l = f.readline()
      lf = l.split(" ")
      ckpt_path = os.path.join(args.checkpoint_dir, os.path.basename(lf[1].strip('"\n')))

  # choose checkpoint which has step of args.inference_step
  else:
    with open(os.path.join(args.checkpoint_dir, "checkpoint"), "r") as f:
      ckpt_path = '%s-%d' % (os.path.join(args.checkpoint_dir, 'model.ckpt'), args.inference_step)
  log('Checkpoint loaded from: %s' % ckpt_path)

  log('Predicting class..')
  with open(os.path.join(args.checkpoint_dir, 'checkpoint'), "r") as f:
    l = f.readline()
    lf = l.split(" ")
    last_ckpt_path = os.path.join(args.checkpoint_dir, os.path.basename(lf[1].strip('"\n')))
  classifier = Classifier()
  classifier.load(last_ckpt_path, num_of_class)

  num_of_train_data = len(wav_features)
  batch_size = int(hparams.batch_size)
  num_of_batches = math.ceil(num_of_train_data / batch_size)
  batch = [0] * batch_size

  csv_writer = open(os.path.join(args.out_dir, "classification_result.csv"), "w")

  # generate classification output from wav files in batches (last batch is excluded)
  for batch_index in tqdm(range(num_of_batches - 1)):
      for element_index in range(batch_size):
          i = (batch_size * batch_index) + element_index
          batch[element_index] = wav_features[i]
      classes = classifier.classify(batch)

      # write down the prediction result to csv file
      for element_index in range(batch_size):
          i = (batch_size * batch_index) + element_index
          output_class = np.argmax(np.asarray(classes[element_index]))
          output_class_name = class_info[output_class][1]
          csv_writer.write(str.format("%s|%s\n" % (wav_filenames[i], output_class_name)))

  # generate classification output from wav files in last batch
  last_batch_size = num_of_train_data - batch_size * (num_of_batches - 1)
  if last_batch_size != 0:
      batch = [0] * last_batch_size
      for element_index in range(last_batch_size):
          i = batch_size * (num_of_batches - 1) + element_index
          batch[element_index] = wav_features[i]
      classes = classifier.classify(batch)

      # write down the prediction result to csv file
      for element_index in range(last_batch_size):
          i = batch_size * (num_of_batches - 1) + element_index
          output_class = np.argmax(np.asarray(classes[element_index]))
          output_class_name = class_info[output_class][1]
          csv_writer.write(str.format("%s|%s\n" % (wav_filenames[i], output_class_name)))

  csv_writer.close()
  log('Classification results are saved to: %s' % os.path.join(args.out_dir, "classification_result.csv"))
  sys.exit()



def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-in', '--input_dir', required=True, default='./wavs')
  parser.add_argument('-out', '--out_dir', default='./classification_result')
  parser.add_argument('-checkpoint', '--checkpoint_dir', default='./log_dir')
  parser.add_argument('-is', '--inference_step', type=int,
    help='Global step to infer classification.')
  args = parser.parse_args()
  inference(args)


if __name__ == "__main__":
  main()
