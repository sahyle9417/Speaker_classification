import argparse
from datetime import datetime
import math
import os
import subprocess
import time
import tensorflow as tf
import traceback
import numpy as np
import shutil
import sys
import glob
from tqdm import tqdm

from util import audio
from util import infolog, ValueWindow
from hparams import hparams, hparams_debug_string
from datafeeder import DataFeeder
from models import create_model

log = infolog.log


def get_git_commit():
  subprocess.check_output(['git', 'diff-index', '--quiet', 'HEAD'])   # Verify client is clean
  commit = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode().strip()[:10]
  log('Git commit: %s' % commit)
  return commit


def add_stats(model):
  with tf.variable_scope('stats') as scope:
    tf.summary.histogram('outputs', model.outputs)
    tf.summary.histogram('targets', model.targets)
    tf.summary.scalar('learning_rate', model.learning_rate)
    tf.summary.scalar('loss', model.loss)
    return tf.summary.merge_all()


def time_string():
  return datetime.now().strftime('%Y-%m-%d %H:%M')


def train(args):
  commit = get_git_commit() if args.git else 'None'
  checkpoint_path = os.path.join(args.log_dir, 'model.ckpt')
  log('Checkpoint path: %s' % checkpoint_path)

  input_dir = os.path.abspath(os.path.expanduser(args.input_dir))
  input_dir_sublist = os.listdir(input_dir)
  class_names = []
  for i in range(len(input_dir_sublist)):
    if os.path.isdir(os.path.join(input_dir, input_dir_sublist[i])) and input_dir_sublist[i] != 'feat':
      class_names.append(input_dir_sublist[i])
  class_names.sort()
  num_of_class = len(class_names)

  csv_writer = open(os.path.join(args.log_dir, "class_info.csv"), "w")
  for i in range(num_of_class):
    csv_writer.write(str.format("%d|%s\n" % (i, class_names[i])))
  csv_writer.close()

  wav_file_paths = []
  for class_num in range(num_of_class):
    for (path) in os.walk(os.path.join(input_dir, class_names[class_num])):
      for j in range(len(path[2])):
        wav_file_paths.append([class_num, os.path.join(path[0], path[2][j])])

  feat_dir = os.path.join(input_dir,'feat')
  os.makedirs(feat_dir, exist_ok=True)

  if len(glob.glob(os.path.join(feat_dir, '*.npz'))) < len(wav_file_paths):
    for i in tqdm(range(len(wav_file_paths))):
      feat_path = wav_file_paths[i][1][:-4]                    # trim .wav
      feat_path = feat_path.replace(input_dir, '').lstrip('/') # trim path above input_dir
      feat_path = feat_path.replace(os.path.sep, '___')
      feat_path = os.path.join(feat_dir, feat_path + '.npz')
      feature = audio.extract_feature(wav_file_paths[i][1])
      np.savez(feat_path, label=np.asarray([wav_file_paths[i][0]], dtype=np.int32), feature=feature.astype(np.float32), allow_pickle=False)

  log('Features loaded from: %s' % feat_dir)
  log('Using model: %s' % args.log_dir)
  log(hparams_debug_string())

  # Set up DataFeeder:
  coord = tf.train.Coordinator()
  with tf.variable_scope('datafeeder') as scope:
    feeder = DataFeeder(coord, feat_dir, class_names, hparams)
  # Set up model:
  global_step = tf.Variable(0, name='global_step', trainable=False)
  with tf.variable_scope('model') as scope:
    model = create_model(hparams)
    model.initialize(feeder.input_features, feeder.num_of_class, feeder.targets)
    model.add_loss()
    model.add_optimizer(global_step)
    stats = add_stats(model)

  # Bookkeeping:
  step = 0
  time_window = ValueWindow(100)
  loss_window = ValueWindow(100)
  saver = tf.train.Saver(max_to_keep=5, keep_checkpoint_every_n_hours=2)


  # Train!
  config = tf.ConfigProto()
  config.gpu_options.allow_growth=True
  with tf.Session(config=config) as sess:
    try:
      summary_writer = tf.summary.FileWriter(args.log_dir, sess.graph)
      sess.run(tf.global_variables_initializer())

      if args.restore_step:
        # Restore from a checkpoint if the user requested it.
        if args.restore_step == -1: # 마지막 저장된 checkpoint부터 학습 재개
            with open(os.path.join(args.log_dir,"checkpoint"),"r") as f:
                l = f.readline()
                lf = l.split(" ")
                restore_path = os.path.join(args.log_dir, os.path.basename(lf[1].strip('"\n')))
        else:
            restore_path = '%s-%d' % (checkpoint_path, args.restore_step)
        saver.restore(sess, restore_path)
        log('Resuming from checkpoint: %s at commit: %s' % (restore_path, commit), slack=True)
      else:
        log('Starting new training run at commit: %s' % commit, slack=True)


      feeder.start_in_session(sess)

      while not coord.should_stop():
        start_time = time.time()
        step, loss, opt, output, target = sess.run([global_step, model.loss, model.optimize, model.outputs, model.targets])
        # print("[{}] output={} target={}".format(
        #   np.argmax(np.asarray(output[0]))==np.argmax(np.asarray(target[0])),
        #   np.argmax(np.asarray(output[0])), np.argmax(np.asarray(target[0]))))
        time_window.append(time.time() - start_time)
        loss_window.append(loss)
        message = 'Step %-7d [%.03f sec/step, loss=%.07f, avg_loss=%.07f]' % (
          step, time_window.average, loss, loss_window.average)
        if step % 10 == 0:
          log(message, slack=(step % args.checkpoint_interval == 0))

        if loss > 100 or math.isnan(loss):
          log('Loss exploded to %.05f at step %d!' % (loss, step), slack=True)
          raise Exception('Loss Exploded')

        if step % args.summary_interval == 0:
          log('Writing summary at step: %d' % step)
          summary_writer.add_summary(sess.run(stats), step)

        if step % args.checkpoint_interval == 0:
          log('Saving checkpoint to: %s-%d' % (checkpoint_path, step))
          saver.save(sess, checkpoint_path, global_step=step)

        if step >= args.max_step:
          log('Arrived at maximum training step %s' % (step))
          coord.request_stop()
          sys.exit()

    except Exception as e:
      log('Exiting due to exception: %s' % e, slack=True)
      traceback.print_exc()
      coord.request_stop(e)


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-in', '--input_dir', required=True, default='./input_dir')
  parser.add_argument('-out', '--log_dir', default='./log_dir')
  parser.add_argument('-hp', '--hparams', default='',
    help='Hyperparameter overrides as a comma-separated list of name=value pairs')
  parser.add_argument('-rs', '--restore_step', type=int,
    help='Global step to restore from checkpoint.')
  parser.add_argument('-ms', '--max_step', type=int, default=50000,
    help='Maximum step to train.')
  parser.add_argument('--summary_interval', type=int, default=5000,
    help='Steps between running summary ops.')
  parser.add_argument('--checkpoint_interval', type=int, default=5000,
    help='Steps between writing checkpoints.')
  parser.add_argument('--slack_url',
    help='Slack webhook URL to get periodic reports.')
  parser.add_argument('--tf_log_level', type=int, default=1,
    help='Tensorflow C++ log level.')
  parser.add_argument('--git', action='store_true',
    help='If set, verify that the client is clean.')
  args = parser.parse_args()
  os.environ['TF_CPP_MIN_LOG_LEVEL'] = str(args.tf_log_level)
  args.input_dir = args.input_dir.rstrip('/')	# remove / at the end of the dir name
  args.log_dir = args.log_dir.rstrip('/')
  os.makedirs(args.log_dir, exist_ok=True)
  infolog.init(os.path.join(args.log_dir, 'train.log'), args.log_dir, args.slack_url)
  hparams.parse(args.hparams)
  train(args)


if __name__ == '__main__':
  main()
