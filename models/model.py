import tensorflow as tf
from util.infolog import log
from .modules import fully_connected, fc_with_dropout


class Model():
  def __init__(self, hparams):
    self._hparams = hparams

  def initialize(self, input_features, num_of_class, targets=None):
    with tf.variable_scope('inference') as scope:
      is_training = targets is not None
      hp = self._hparams
      neurons_in_layers = hp.neurons_in_layers
      self.num_of_class = num_of_class
      self.batch_size = hp.batch_size
      self.input_features = input_features
      is_training = targets is not None
      self.outputs = fc_with_dropout(self.input_features, neurons_in_layers, self.num_of_class, is_training, scope=None)
      if targets is not None:
        self.targets = tf.one_hot(tf.convert_to_tensor(targets, dtype=tf.int32),self.num_of_class) # [B, num_of_class]


  def add_loss(self):
    '''Adds loss to the model. Sets "loss" field. initialize must have been called.'''
    with tf.variable_scope('loss') as scope:
      self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=self.targets, logits=self.outputs))


  def add_optimizer(self, global_step):
    '''Adds optimizer. Sets "gradients" and "optimize" fields. add_loss must have been called.

    Args:
      global_step: int32 scalar Tensor representing current global step in training
    '''
    with tf.variable_scope('optimizer') as scope:
      hp = self._hparams
      if hp.decay_learning_rate:
        self.learning_rate = _learning_rate_decay(hp.initial_learning_rate, global_step)
      else:
        self.learning_rate = tf.convert_to_tensor(hp.initial_learning_rate)
      optimizer = tf.train.AdamOptimizer(self.learning_rate, hp.adam_beta1, hp.adam_beta2)
      gradients, variables = zip(*optimizer.compute_gradients(self.loss))
      self.gradients = gradients
      clipped_gradients, _ = tf.clip_by_global_norm(gradients, 1.0)

      # Add dependency on UPDATE_OPS; otherwise batchnorm won't work correctly. See:
      # https://github.com/tensorflow/tensorflow/issues/1122
      with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        self.optimize = optimizer.apply_gradients(zip(clipped_gradients, variables),
          global_step=global_step)


def _learning_rate_decay(init_lr, global_step):
  # Noam scheme from tensor2tensor:
  warmup_steps = 4000.0
  step = tf.cast(global_step + 1, dtype=tf.float32)
  return init_lr * warmup_steps**0.5 * tf.minimum(step * warmup_steps**-1.5, step**-0.5)
