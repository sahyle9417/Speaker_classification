import tensorflow as tf
from hparams import hparams, hparams_debug_string


def fully_connected(inputs, neurons_in_layers, num_of_class, scope=None):
  x = inputs
  with tf.variable_scope(scope or 'fully_connected'):
    for i, neurons in enumerate(neurons_in_layers):
      x = tf.layers.dense(x, units=neurons, activation=tf.nn.relu, name='dense_%d' % (i+1))
    output = tf.layers.dense(x, units=num_of_class, activation=None, name='dense_last')
  return output


def fc_with_dropout(inputs, neurons_in_layers, num_of_class, is_training, scope=None):
  x = inputs
  drop_rate = hparams.drop_rate if is_training else 0.0
  with tf.variable_scope(scope or 'fc_with_dropout'):
    for i, neurons in enumerate(neurons_in_layers):
      x = tf.layers.dense(x, units=neurons, activation=tf.nn.relu, name='dense_%d' % (i+1))
      x = tf.layers.dropout(x, rate=drop_rate, name='dropout_%d' % (i+1))
    output = tf.layers.dense(x, units=num_of_class, activation=None, name='dense_last')
  return output

