import tensorflow as tf

# Default hyperparameters:
hparams = tf.contrib.training.HParams(

  # Audio
  sample_rate=22050,

  # Train
  batch_size = 32,
  batches_per_group = 32,
  initial_learning_rate = 0.002,
  drop_rate = 0.5,
  adam_beta1 = 0.9,
  adam_beta2 = 0.999,
  decay_learning_rate = True,

  # Model
  wav_features = 193,
  neurons_in_layers = [193, 193],
  #num_of_class = 2
)


def hparams_debug_string():
  values = hparams.values()
  hp = ['  %s: %s' % (name, values[name]) for name in sorted(values)]
  return 'Hyperparameters:\n' + '\n'.join(hp)
